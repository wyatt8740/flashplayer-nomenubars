# Flashplayer-menuless

When Adobe's standalone Flash player projector is run in full-screen on Linux,
keyboard controls no longer work. And even if window decorations are turned off
on the player normally, the menubar is still visible. Thus, a small hack is
needed to prevent the manifestation of the kinds of GTK+ 2 panels that
flashplayer attempts to create.

Once this hack is active, a flexible window manager can be set to maximize the
window and hide all window decorations, effectively equating to full-screen
mode but with a fully functional keyboard.

I use FVWM, and already had it configured so a keyboard shortcut could turn
off all decorations for a given window. Other WM's may have different
mechanisms for doing this.

It's also nice for just having a less-cluttered window in general.

### Installation

```
make
make install
```
will put the library in `$INSTDIR/lib` and `flashplayer_nomenu` in
`$INSTDIR/bin`. $INSTDIR defaults to `/usr/local`.
To change the install path, edit the Makefile or run
`make install INSTDIR=/path/to/install`.
Then, you can run flash, either with `flashplayer_nomenu` (a wrapper script;
treat it like invoking `flashplayer` from the CLI normally) or manually with:

```
LD_PRELOAD=/path/to/lib/flash_nomenus_override.so flashplayer [args]
```

Do note that the script assumes `flashplayer` is the name of your projector
binary, and that it is in a directory in your $PATH variable.

Ignore the console warnings about stuff like `invalid (NULL) pointer instance`;
this is a side-effect of the library stub and shouldn't impact the ability to
play flash files. Or add `2>/dev/null` to the beginning of the line in the
script that invokes the player to hide the warning messages.

Either pass the .swf as a command-line argument, or use
`ctrl-O` to open one after launching the player.

### A few details (what I learned doing this)
This was my first time trying to substitute functions in C; I'd seen it
done before for things like ['gtk3-nocsd'](https://github.com/PCMan/gtk3-nocsd)
but I had never tried to do it myself. It was surprisingly simple, actually;
more than I'd anticipated it being.

This hack is accomplished by using LD_PRELOAD to inject our own stub
functions over the ones that would normally be responsible for creating
certain GTK widgets. Due to LD_PRELOAD, our own small stub functions take
precedence over the original libgtk's function implementations.

Since GTK+ 2 programs typically use `g_free()` to free allocated memory, and
`g_free()` does nothing if given a null pointer, it is safe in this
case to just have the stub functions return 'null' (zero) without ever
`malloc`'ing anything as the real functions would normally do.

In my experience this has worked fine.

I don't expect others will use this, but if you do, have fun!

### a workaround for some common window managers
For window managers that don't let users turn off decorations in specific
windows, you can try to build the program using `CFLAGS=-DFULLSCREEN make`
instead of normally running `make`. If you have already run `make` normally,
you will need to first do a `make clean` before the rebuild will work.

If you do add this flag, the injecting library will also override the
function used to exit from fullscreen so that the window stays undecorated.
This is a pretty ugly hack, so it is not on by default. To use it, you must
enter fullscreen normally (`ctrl+F`) and then hit `escape`. Instead of exiting
fullscreen, this will _technically_ pop flash player out of fullscreen mode
and then request the window manager hide decorations and maximize the window.
This results in pseudo-fullscreen, but with working keyboard shortcuts. To
exit the window you will need to use `ctrl+Q` or your window manager's
dedicated shortcut (for instance, `alt+f4` works in Metacity).

Again, this is very ugly, so if your WM has a mechanism for users to manually
un-decorate and maximize windows, I recommend doing that instead; I had
someone ask me for help making Metacity work with this so I did it for them
and they report it works well enough there.
